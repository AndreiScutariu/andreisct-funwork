﻿namespace Mediator
{
    using System.Collections.Generic;
    using System.Reflection;

    using Autofac;
    using Autofac.Features.Variance;

    using Handlers;
    using Handlers.User;

    using MediatR;

    public static class ContainerBuilderExtensions
    {
        public static void RegisterMediatorHandlers(this ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(UserCommandsHandler).GetTypeInfo().Assembly).AsImplementedInterfaces();

            builder.Register<SingleInstanceFactory>(
                ctx =>
                    {
                        var c = ctx.Resolve<IComponentContext>();
                        return t => c.Resolve(t);
                    });

            builder.Register<MultiInstanceFactory>(
                ctx =>
                    {
                        var c = ctx.Resolve<IComponentContext>();
                        return t => (IEnumerable<object>)c.Resolve(typeof(IEnumerable<>).MakeGenericType(t));
                    });
        }
    }
}