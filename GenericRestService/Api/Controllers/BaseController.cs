﻿namespace Api.Controllers
{
    using System.Web.Http;

    using MediatR;

    public abstract class BaseController : ApiController
    {
        protected BaseController(IMediator mediator)
        {
            Mediator = mediator;
        }

        protected IMediator Mediator { get; }
    }
}