﻿namespace Api.Controllers
{
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Api.Infrastructure.AutoMapper.Extensions;
    using Api.Models;

    using MediatR;

    using Messages.Queries;

    public class UserController : BaseController
    {
        public UserController(IMediator mediator)
            : base(mediator)
        {
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Get()
        {
            var query = new GetUserQuery();
            var response = await Mediator.SendAsync(query);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> Get(int id)
        {
            var query = new GetUserQuery { Id = id };
            var response = await Mediator.SendAsync(query);

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post([FromBody] CreateUserRequest request)
        {
            var command = request.MapToCommand();
            await Mediator.SendAsync(command);

            return Request.CreateResponse(HttpStatusCode.Created);
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(int id, [FromBody] CreateUserRequest request)
        {
            var command = request.MapToCommand(id);
            await Mediator.SendAsync(command);

            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}