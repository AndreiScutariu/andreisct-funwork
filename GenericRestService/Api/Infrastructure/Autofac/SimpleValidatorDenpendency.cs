﻿namespace Api.Infrastructure.Autofac
{
    public interface IValidatorDependency
    {
        int PasswordLenght { get; }
    }

    public class SimpleValidatorDenpendency : IValidatorDependency
    {
        public int PasswordLenght => 8;
    }
}