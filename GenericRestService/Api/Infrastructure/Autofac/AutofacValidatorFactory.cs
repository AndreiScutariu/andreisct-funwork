namespace Api.Infrastructure.Autofac
{
    using System;

    using FluentValidation;

    using global::Autofac;

    public class AutofacValidatorFactory : ValidatorFactoryBase
    {
        private readonly IComponentContext context;

        public AutofacValidatorFactory(IComponentContext context)
        {
            this.context = context;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            object instance;

            if (!context.TryResolve(validatorType, out instance))
            {
                return null;
            }

            return instance as IValidator;
        }
    }
}