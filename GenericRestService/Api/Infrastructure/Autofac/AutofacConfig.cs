﻿namespace Api.Infrastructure.Autofac
{
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Http.Validation;

    using FluentValidation;
    using FluentValidation.WebApi;

    using global::Autofac;
    using global::Autofac.Integration.WebApi;

    using Mediator;

    using Owin;

    public static class AutofacConfig
    {
        public static void Initialize(HttpConfiguration config, IAppBuilder app)
        {
            var builder = new ContainerBuilder();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).InstancePerRequest();

            builder.RegisterMediatorHandlers();

            builder.RegisterValidators(config);

            var container = builder.Build();

            container.RegisterModelValidatorProvider(config);

            var autofacWebApiDependencyResolver = new AutofacWebApiDependencyResolver(container);
            config.DependencyResolver = autofacWebApiDependencyResolver;
            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
        }
    }

    internal static class ContainerBuilderExtensions
    {
        public static void RegisterValidators(this ContainerBuilder builder, HttpConfiguration config)
        {
            builder.RegisterType<SimpleValidatorDenpendency>().As<IValidatorDependency>().InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.Contains("Validator"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterType<AutofacValidatorFactory>().As<IValidatorFactory>().SingleInstance();
        }

        public static void RegisterModelValidatorProvider(this IContainer container, HttpConfiguration config)
        {
            var provider = new FluentValidationModelValidatorProvider(container.Resolve<IValidatorFactory>());
            config.Services.Add(typeof(ModelValidatorProvider), provider);
        }
    }
}