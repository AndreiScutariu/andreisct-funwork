﻿namespace Api.Infrastructure.AutoMapper
{
    using Api.Models;

    using global::AutoMapper;

    using Messages.Commands;

    public static class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(
                cfg =>
                    {
                        cfg.CreateMap<CreateUserRequest, CreateUserCommand>();
                        cfg.CreateMap<CreateUserRequest, UpdateUserCommand>();
                    });
        }
    }
}