﻿namespace Api.Infrastructure.AutoMapper.Extensions
{
    using Api.Models;

    using global::AutoMapper;

    using Messages.Commands;

    public static class MapperExtension
    {
        public static CreateUserCommand MapToCommand(this CreateUserRequest model)
        {
            return Mapper.Map<CreateUserCommand>(model);
        }

        public static UpdateUserCommand MapToCommand(this CreateUserRequest model, int id)
        {
            var command = Mapper.Map<UpdateUserCommand>(model);
            command.Id = id;
            return command;
        }
    }
}