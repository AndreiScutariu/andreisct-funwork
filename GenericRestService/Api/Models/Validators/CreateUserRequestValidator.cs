﻿namespace Api.Models.Validators
{
    using Api.Infrastructure.Autofac;

    using FluentValidation;

    public class CreateUserRequestValidator : AbstractValidator<CreateUserRequest>
    {
        public CreateUserRequestValidator(IValidatorDependency dependency)
        {
            RuleFor(customer => customer.FullName).NotEmpty();
            RuleFor(customer => customer.Password).Length(dependency.PasswordLenght);
            RuleFor(customer => customer.Email).EmailAddress();
        }
    }
}