﻿using Api;

using Microsoft.Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Api
{
    using System.Web.Http;
    using System.Web.Http.ExceptionHandling;

    using Api.Infrastructure.Autofac;
    using Api.Infrastructure.AutoMapper;
    using Api.Infrastructure.Filters;

    using Owin;

    using ExceptionLogger = Api.Infrastructure.Filters.ExceptionLogger;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            config.EnableCors();

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.Filters.Add(new ValidateModelFilterAttribute());

            config.Services.Replace(typeof(IExceptionLogger), new ExceptionLogger());

            app.UseWebApi(config);

            AutofacConfig.Initialize(config, app);

            AutoMapperConfig.Initialize();
        }
    }
}