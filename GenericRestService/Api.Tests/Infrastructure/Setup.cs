﻿namespace Api.Tests.Infrastructure
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;

    public static class Setup
    {
        public static HttpClient GetHttpClient()
        {
            var client = new HttpClient { BaseAddress = new Uri("http://localhost/service.user/") };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}