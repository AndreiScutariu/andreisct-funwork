﻿namespace Api.Tests.Infrastructure
{
    using System.Net.Http;
    using System.Text;

    using Api.Models;

    using Newtonsoft.Json;

    public static class RequestExtensions
    {
        private static string ToJson<T>(this T request)
        {
            return JsonConvert.SerializeObject(request);
        }

        public static StringContent ToJsonContent<T>(this T request)
        {
            return new StringContent(request.ToJson(), Encoding.UTF8, "application/json");
        }
    }
}