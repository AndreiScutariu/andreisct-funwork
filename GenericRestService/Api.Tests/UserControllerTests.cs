﻿namespace Api.Tests
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    using Api.Models;
    using Api.Tests.Infrastructure;

    using FluentAssertions;

    using Microsoft.Owin.Testing;

    using NUnit.Framework;

    public class TestStartup : Startup
    {
        public TestStartup()
        {
        }
    }

    [TestFixture]
    public class UserControllerTests
    {
        private HttpClient client;

        [SetUp]
        public void SetUp()
        {
            client = Setup.GetHttpClient();
        }

        [Test]
        public async Task ShouldReturnBadRequestWhenPostEmptyClient()
        {
            var request = new CreateUserRequest();

            var value = request.ToJsonContent();
            var result = await client.PostAsync(new Uri("api/user/", UriKind.Relative), value);

            result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Test]
        public async Task ShouldReturnCreatedStatusCodeWhenPostValidClient()
        {
            var request = new CreateUserRequest { Email = "andrei@somthing.com", FullName = "name", Password = "pass1234" };

            var value = request.ToJsonContent();
            var result = await client.PostAsync(new Uri("api/user/", UriKind.Relative), value);

            result.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Test]
        public async Task ShouldReturnBadRequestWhenPostClientWithInvalidPassword()
        {
            var request = new CreateUserRequest { Email = "andrei@somthing.com", FullName = "name", Password = "pass12" };

            var value = request.ToJsonContent();
            var result = await client.PostAsync(new Uri("api/user/", UriKind.Relative), value);

            result.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Test]
        public async Task ShouldReturnOkWhenGetClient()
        {
            var result = await client.GetAsync(new Uri("api/user/", UriKind.Relative));

            result.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [TearDown]
        public void TearDown()
        {
            client.Dispose();
        }
    }
}