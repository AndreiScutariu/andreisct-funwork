namespace Messages.Queries.Model
{
    public class GetUserQueryResponse
    {
        public string FullName { get; set; }

        public string Email { get; set; }
    }
}