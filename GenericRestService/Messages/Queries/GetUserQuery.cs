﻿namespace Messages.Queries
{
    using System.Collections.Generic;

    using MediatR;

    using Messages.Queries.Model;

    public class GetUserQuery : IAsyncRequest<IEnumerable<GetUserQueryResponse>>
    {
        public int Id { get; set; }
    }
}