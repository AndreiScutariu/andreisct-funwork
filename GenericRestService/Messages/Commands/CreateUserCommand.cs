﻿namespace Messages.Commands
{
    using MediatR;

    public class CreateUserCommand : IAsyncRequest
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}