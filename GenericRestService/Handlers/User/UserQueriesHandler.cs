﻿namespace Handlers.User
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using MediatR;

    using Messages.Queries;
    using Messages.Queries.Model;

    public class UserQueriesHandler : IAsyncRequestHandler<GetUserQuery, IEnumerable<GetUserQueryResponse>>
    {
        public async Task<IEnumerable<GetUserQueryResponse>> Handle(GetUserQuery message)
        {
            return await Task.Run(() => GetUsers(message));
        }

        private IEnumerable<GetUserQueryResponse> GetUsers(GetUserQuery message)
        {
            yield return new GetUserQueryResponse { FullName = message.Id.ToString() };
        }
    }
}