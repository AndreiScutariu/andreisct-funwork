﻿namespace Handlers.User
{
    using System.Threading.Tasks;

    using MediatR;

    using Messages.Commands;

    public class UserCommandsHandler : IAsyncRequestHandler<CreateUserCommand, Unit>,
                                       IAsyncRequestHandler<UpdateUserCommand, Unit>
    {
        public async Task<Unit> Handle(CreateUserCommand message)
        {
            return await Task.Run(() => new Unit());
        }

        public async Task<Unit> Handle(UpdateUserCommand message)
        {
            return await Task.Run(() => new Unit());
        }
    }
}